/*
 * The prupose of this module is to analyse a given string
 * for being a IDNA domain name and decodes it,
 * it is also able to return an encoded form if given unicode.
 * The result is supposed to be used in a template
 */

use serde::{Deserialize, Serialize};
use ::idna;

#[derive(Deserialize, Serialize, Copy, Default, Clone, PartialEq)]
#[serde(rename_all="lowercase")]
pub enum NameType {
	#[default]
	Ascii,
	Unicode,
	Idn,
}

// Note, that the
#[derive(Deserialize, Serialize, Default, Clone)]
pub struct IdnaName {
	pub unicode: String,
	// if null the unicode version only contains ascii range chars,
	// not neccessary to encode
	pub idn: Option<String>,
	pub original_was: NameType,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub decoder_error: Option<String>,
	#[serde(skip_serializing_if = "Option::is_none")]
	pub encoder_error: Option<String>,
}

impl IdnaName {
	pub fn from_str(s: &str) -> Self {
		if s.is_empty() {
			return Default::default();
		}
		
		let mut original_was = NameType::Unicode;
		let unicode: String;
		let decoder_error;
		if s.starts_with("xn--") && s.is_ascii() {
			original_was = NameType::Idn;
			let (uc, ures) = idna::domain_to_unicode(s);
			unicode = uc;
			decoder_error = ures.map_or_else(|e| Some(e.to_string()), |_| None);
		} else {
			unicode = s.to_owned();
			decoder_error = None;
		};
		let (idn, encoder_error) = match idna::domain_to_ascii(s) {
			Ok(idn) => {
				if idn != s || original_was == NameType::Idn {
					(Some(idn), None)
				} else {
					original_was = NameType::Ascii;
					(None, None)
				}
			},
			Err(e) => {
				(None, Some(e.to_string()))
			}
		};

		IdnaName {
			unicode: unicode,
			idn: idn,
			original_was: original_was,

			decoder_error: decoder_error,
			encoder_error: encoder_error,
		}
	}

	pub fn was_ascii(&self) -> bool {
		return self.original_was == NameType::Ascii;
	}
}

