#!/bin/bash

# Settings variables go here
GEOIP_LICENSE_KEY="$GEOIP_LICENSE_KEY"
PRODUCTS=()
DESTINATION_DIRECTORY="."
DOWNLOAD_LOCATION=""
COLOR_OUTPUT=""
[ -t 1 ] && COLOR_OUTPUT="y"

msg() {
	COLOR=""
	COLOR_RST=""
	if [ -n "$COLOR_OUTPUT" ] ; then
		COLOR_RST="\e[00m"
		case "$2" in
			error|init_error) COLOR="\e[01;31m" ;;
			success) COLOR="\e[32m" ;;
			*) ;;
		esac
	fi
	printf "$COLOR%s$COLOR_RST\n" "$1" >&2
}

show_help() {
cat <<EOF
Usage: maxmind-download.sh [OPTIONS]...

OPTIONS
	--license-key <key>
		Set the licencse key that is unfortunately
		needed for a successful download.
	--product <id>
		Which product to download
		maxmind calls this the EditionID
	--GeoLite2-mmdb-all
		Selects all the GeoLite2 Products in mmdb
		format, hoefully saves some headaces.
		Will download:
		* GeoLite2-ASN
		* GeoLite2-City
		* GeoLite2-Country
	--to <destination>
		Directory to place the downloded files in.
		Filename will be <product>.(mmdb|csv)
	--download-to <destination>
		Directory to download to.
		If specified, the files in the --to directory
		will only be replaced if the download was successful.
	--color
	--no-color Explicitly enable or disable colored output.
	--help Show this help message

ENVOIRNMENT
	GEOIP_LICENSE_KEY can be used to set the licencse key.

EXIT CODES
	1 Invalid paramters or filesystem envoirnment
	2 Download failed
	3 Expected file not found in download
	4 Failed to extract download

EOF
}

while [[ "$#" -gt 0 ]]; do
	case "$1" in
		--license-key) GEOIP_LICENSE_KEY="$2"; shift 2;;
		--product) PRODUCTS+=("$2"); shift 2;;
		--GeoLite2-mmdb-all)
			PRODUCTS+=("GeoLite2-ASN")
			PRODUCTS+=("GeoLite2-City")
			PRODUCTS+=("GeoLite2-Country")
			shift 1;;
		--to) DESTINATION_DIRECTORY="$2"; shift 2;;
		--download-to) DOWNLOAD_LOCATION="$2"; shift 2;;
		--color) COLOR_OUTPUT="y"; shift 1;;
		--no-color) COLOR_OUTPUT=""; shift 1;;

		--help) show_help; exit 0;;
		*) printf "Unknown option: %s\n" "$1"; exit 1;;
	esac
done

if [ -z "$GEOIP_LICENSE_KEY" ] ; then
	msg "No License Key specified, the download won't work this way." init_error
	exit 1
fi

[ -n "$DOWNLOAD_LOCATION" ] || DOWNLOAD_LOCATION="$DESTINATION_DIRECTORY"

if [ -d "$DESTINATION_DIRECTORY" ] || mkdir -p "$DESTINATION_DIRECTORY" ; then
	true
else
	msg "Destination is not a directory and can't be created!" init_error
	exit 1
fi

if [ -d "$DOWNLOAD_LOCATION" ] || mkdir -p "$DOWNLOAD_LOCATION" ; then
	true
else
	msg "Dowload location is not a directory and can't be created!" init_error
	exit 1
fi

if [ "${#PRODUCTS[@]}" -eq "0" ] ; then
	msg "No products specified, nothing to do." init_error
	exit 0
fi

get_product_file_ext() {
	if printf "%s" "$1" | grep -q 'CSV$' ; then
		echo csv
	else
		echo mmdb
	fi
}

# PrductID,DOWNLOAD_LOCATION
download_maxmind_db() {
	msg "Downloading Database $1 …" progess
	# the path to download to
	dl="$2/$1.tar.gz"
	curl -fsSL -m 40 "https://download.maxmind.com/app/geoip_download?edition_id=$1&license_key=$GEOIP_LICENSE_KEY&suffix=tar.gz" > "$dl"
	if [ "_$?" != "_0" ] ; then
		msg "Databse download of $1 failed!" error
		rm "$dl"
		return 2
	fi
	EXT="$(get_product_file_ext "$1")"
	FILE_TO_EXTRACT="$(tar -tzf "$dl" | grep "/$1\.$EXT$")"
	if [ -z "$FILE_TO_EXTRACT" ] ; then
		msg "No .$EXT file found in the downloaded data!" error
		rm "$dl"
		return 3
	fi
	msg "Extracting $FILE_TO_EXTRACT from downloaded archive …" progess
	if tar -C "$2" --strip-components=1 -xzf "$dl" "$FILE_TO_EXTRACT" ; then
		msg "File extracted successfully." success
		rm "$dl"
		return 0
	else
		msg "File extraction failed!" error
		rm "$dl"
		return 4
	fi
}

EXIT_CODE=""
MSG_OUTPUT_TO_LOG="y"
for product in "${PRODUCTS[@]}" ; do
	download_maxmind_db "$product" "$DOWNLOAD_LOCATION"
	RETCODE="$?"

	if [ "_$RETCODE" = "_0" ] ; then
		filename="$product.$(get_product_file_ext "$product")"
		if [ "_$DOWNLOAD_LOCATION" != "_$DESTINATION_DIRECTORY" ] ; then
			msg "Moving destination file …" progess
			if [ -e "$DESTINATION_DIRECTORY/$filename" ] ; then
				[ -e "$DESTINATION_DIRECTORY/$filename.bak" ] && rm "$DESTINATION_DIRECTORY/$filename.bak"
				mv "$DESTINATION_DIRECTORY/$filename" "$DESTINATION_DIRECTORY/$filename.bak"
			fi
			if mv "$DOWNLOAD_LOCATION/$filename" "$DESTINATION_DIRECTORY/$filename" ; then
				msg "File $filename installed successfully." success
			else
				msg "Failed to install $filename!" error
			fi
		fi
	else
		[ -n "$EXIT_CODE" ] && [ "$EXIT_CODE" -lt "$RETCODE" ] || export EXIT_CODE="$RETCODE"
	fi
done

exit $EXIT_CODE
