
use axum::http::status::StatusCode;
use axum::Json;
use axum::response::IntoResponse;
use axum::response::Response;
use axum_extra::extract::cookie::Cookie;
use axum_extra::extract::cookie;
use lib_humus::HumusView;

use crate::DigResult;
use crate::IpResult;
use crate::config::DnsResolverConfig;
use crate::settings::QuerySettings;
use crate::settings::ResponseFormat;


#[derive(serde::Serialize, Clone)]
#[serde(untagged)]
pub enum View {
	Asn { asn: u32 },
	Dig { query: String, result: DigResult },
	DnsResolver{ config: DnsResolverConfig },
	DnsResolverList,
	Index { result: IpResult, user_agent: Option<String> },
	Ip { result: IpResult },
	Message{ title: String, message: String },
	#[serde(rename="404")]
	NotFound,
}

impl HumusView<QuerySettings, ResponseFormat> for View {
	fn get_template_name(&self) -> String {
		match self {
			View::Asn{..} => "asn",
			View::Dig{..} => "dig",
			View::DnsResolver{..} => "dns_resolver",
			View::DnsResolverList => "dns_resolver_list",
			View::Index{..} => "index",
			View::Ip{..} => "ip",
			View::Message{..} => "message",
			View::NotFound => "404",
		}.to_string()
	}

	fn get_status_code(&self, _: &QuerySettings) -> StatusCode {
		match self {
			Self::NotFound => StatusCode::NOT_FOUND,
			_ => StatusCode::OK,
		}
	}
	
	fn get_cookie_header(&self, settings: &QuerySettings) -> Option<String> {
		Some(
			Cookie::build(Cookie::new("dns_resolver",settings.dns_resolver_id.to_string()))
				.path("/")
				.same_site(cookie::SameSite::Strict)
				.build()
				.to_string()
		)
	}
		
	fn get_api_response(self, settings: &QuerySettings) -> Response {
		match self {
			Self::Dig{result, ..} => {
				Json(result).into_response()
			},
			Self::Index{result, ..} | Self::Ip{result, ..} => {
				Json(result).into_response()
			},
			Self::DnsResolverList => {
				Json(settings.available_dns_resolvers.clone()).into_response()
			},
			Self::DnsResolver{ config } => {
				Json(config).into_response()
			}
			_ => Json(self).into_response(),
		}
	}
}

