# Configuring echoip-slatecave

The configuration of echoip-slatecave consists of one [toml](https://toml.io) file for configuring the service itself and one for configuring the template. This document covers on the service configuration.

By default the service tried to load `echoip.toml` from the current pwd as its configurtion, a custom path can be specified using the `-c <configfile>` option.

The configuration file consists of multiple sections of options grouped by feature.

* [server](#server) - Contains global options for the service
* [dns](#dns)
* [geoip](#geoip)
* [template](#template) - templating and rendering
* [ratelimit](#ratelimit)

The default configuration tries to be working out of the box while providing as little security footguns as possible. Have a look at the `server` and `dns` sections that is where most security related options are.

## [server]

### `listen_on`

Configures the bind address and port the service will listen on.

It uses the format `<ip-address>:<port>`, for the ip-address use `127.0.0.1` for only allowing local connections or `0.0.0.0` for allowing IPv4 connections from anywhere, `[::]` for allowing IPv6 connections from anywhere. For the port pick a free TCP port on your machine, default is `3000`.

This option can be overridden by the `-l <ip-address>:<port>` option on the commandline.

### `ip_header`

Configures which http header that contains the real client IP-Address or tells the service to use the IP-Address used to connect to it when in use without a proxy server.

Possible values are best covered by the [documentation for the underlying datatype](https://docs.rs/axum-client-ip/latest/axum_client_ip/enum.SecureClientIpSource.html).

When using a reverse Proxy `RightmostXForwardedFor` is usually what you want.

When using without a reverse Proxy set to `ConnectInfo`.

Please keep in mind that the ratelimit depends on the IP-Address being non-spoofable which is only given if the setting here matches the one of your proxy.

### `allow_private_ip_lookup`

Defaults to `false`, set to `true` to allow looking up IP-Addresses that fall into the private IP-Range. Enabling is not recommended when the server is publically accessible.

### `static_location`

When specified allows overriding the location where echoip-slatecave serve static files from (the default is the `static` directory under the [template_location](#template_location) )

## [dns]

### `allow_forward_lookup`

When set to `true`, allows resolving Domain names over the webinterface for every configured dns resolver.

### `allow_reverse_lookup`

When set to `true`, allows looking up domain names for IP-Addresses using reverse dns lookups for every configured resolver.

### `hidden_suffixes`

Configure it with a list of suffixes of domin names you don't want to leak out to the web interface.

If an entry matches, echoip-slatecave will try its best to pretend that resolving the name resulted in it not existing.

Example:
```toml
[dns]
hidden_suffixes = [".local",".box",".internal"]
```

This configuration option will not be exposed over the webinterface.

### System Resolver

By default echoip-slatecave uses the system configuration for dns like most other programs.

In case this is undesired one can disable it by setting `enable_system_resolver` to false.

```toml
[dns]
enable_system_resolver = false
```

In case you want to use the system resolver and customize it.

`system_resolver_name`
: Equivalent to the `display_name` of a custom resolver, default: "System"

`system_resolver_id`
: Equivalent to the `key` of a custom resolver, default "system"

`system_resolver_weight`
: Equivalent to the `weight` of a custom resolver, default: 1000

### Custom resolvers 

It is possible to confgure custom resolvers in plce of or in addition to the default system resolver.

To do this create a new section in the configuration file called `[dns.resolver.<key>]` where `<key>` is an url-friendly name for the resolver.

In this section one can fonfigure functional and cosmetic aspects of the resolver.

`display_name`
: The name that will be used for this resolver in the UI, it should be short and descriptive. Naming a main feature helps with keeping track of which Server is which when you have multiple servers with similar names. (required)

`info_url`
: A url pinting to the page containing service information and a technical overview of the dns server. (optional)

`aliases`
: A list of short strings that can be used to quickly typing in the desired server, inteded for power users.

`weight`
: An integer that helps with ranking multiple resolvers, oneswith higher weights will be displayed further up the lists of available options, the one with the highest weight will become the default resolver.

`servers`
: A list of socket addresses, that is `<ip-address>:<port>` pointing to the available dns servers. Leaving the port out is not yet supported.

`protocol`
: Which protocol to use for accessing the dns server. While `udp` works for almost all servers using `tls` is recommended when available.

`tls_dns_name`
: When using `tls`, `https` or `quic` this name helps the server knowing want ([SNI](https://en.wikipedia.org/wiki/Server_Name_Indication)). Usually this is the domain name of the dns server.

Available protocols:

| Protocol | Default Port |
|----------|--------------|
| udp      | 53           |
| tcp      | 53           |
| tls      | 853          |
| https    | 443          |
| quic     | 443          |


Example configuration:

```toml
[dns.resolver.digitalcourage]
display_name = "Digitalcourage"
info_url = "https://digitalcourage.de/support/zensurfreier-dns-server"
aliases = ["dc","dc3","digitalcourage3"]
weight = 990

servers = ["5.9.164.112:853","[2a01:4f8:251:554::2]:853"]
protocol = "tls"
tls_dns_name = "dns3.digitalcourage.de"
```

## [geoip]

These options configure paths to maxmind (or compatible) databses. The Official databases are available after signing up on [maxmind.com](https://maxmind.com). (In case someone knows a similar source of IP to geolocation mapping under a less propritetary license please contact me.)

To get the full functionalityyou need the ASN and City databases in mmdb format.

The `asn_database` and `location_database` fields are for their filepaths.

Example:
```toml
[geoip]
asn_database = "mmdb/GeoLite2-ASN.mmdb"
location_database = "mmdb/GeoLite2-City.mmdb"
```

Note: When echoip-slatecave rececieves a `SIGUSR1` posix signal it will attempt to reload the mmdb files. This is useful for keeping the databses up to date without having to restart the service.

## [template]

### `template_location`

This option contains the path to the direcotry containing the templates. It is overridden by the `-t <template-location>` command line option.

It can contain a glob pattern, bit make sure to configure the [`static_location`](#static_location) if it does.

### `extra_config`

This points to the toml file containing the configuration for the template itself, its content depends on what the template expects. This option is overridden by the `-e <extra-config>` command line option.

### `text_user_agents`

A list of Prefixes of UserAgents that should be served plain text by default.

Example:
```toml
[template]
# Give curl the plain text version by default
text_user_agents = ["curl/"]
```

## [ratelimit]

Configure a Quota for the Rate limiter.

Note: The ratelimiter depends on the [ip_header](#ip_header) setting to be coorect, otherwise spoofing is possible enabling Denail of Service type attacks.

`per_minute`
: Integer of how many requests are allowed (and regnerate) per minute.

`burst`
: Integer of how many requests are additionally allowed.

Note: The ratelimit is implemented using the [governor](https://docs.rs/governor/0.6.0/governor/) crate.

Example:

```toml
[ratelimit]
per_minute = 20
burst = 15
```

This allows up to 20+15=35 rquests without running into any limit. For every 3 seconds passing one additional request is granted (up to the limit of 35), which amounts to 60/3 = 20 requests per minute.
