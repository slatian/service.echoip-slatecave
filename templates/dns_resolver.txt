# DNS Resolver: {{ data.config.display_name }}

{%- set c = data.config %}
{% if c.servers | length == 1 %}
Address: {{ c.servers | first }}
{% elif c.servers | length > 1 %}
Addresses:
{%- for a in c.servers %}
* {{a}}
{%- endfor %}
{%- else %}
Address: None Configured
{%- endif %}

Protocol: {{ c.protocol }}
{%-if c.tls_dns_name %}
DNS Name: {{ c.tls_dns_name }}
{%- endif %}
{%- if c.aliases | length == 1 %}
Alias: {{ c.aliases | first }}
{%- elif c.aliases | length > 1 %}
Aliases:
{%- for a in c.aliases %}
* {{a}}
{%- endfor %}
{%- endif %}
{%- if c.info_url %}

=> {{ c.info_url }}
{%- endif %}
