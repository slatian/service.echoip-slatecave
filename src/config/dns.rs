use serde::{Deserialize,Serialize};
use hickory_resolver::config::Protocol;
use hickory_resolver::config::ResolverConfig as HickoryResolverConfig;
use hickory_resolver::config::NameServerConfig;

use std::sync::Arc;
use std::collections::HashMap;
use std::net::SocketAddr;

#[derive(Deserialize, Clone)]
#[serde(default)]
pub struct DnsConfig {
	pub allow_forward_lookup: bool,
	pub allow_reverse_lookup: bool,
	pub hidden_suffixes: Vec<String>,
	pub resolver: HashMap<Arc<str>,DnsResolverConfig>,

	pub enable_system_resolver: bool,
	pub system_resolver_name: Arc<str>,
	pub system_resolver_weight: i32,
	pub system_resolver_id: Arc<str>,
}

#[derive(Deserialize, Serialize, Clone)]
#[serde(rename_all="lowercase")]
pub enum DnsProtocol {
	Udp,
	Tcp,
	Tls,
	Https,
	Quic,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct DnsResolverConfig {
	pub display_name: Arc<str>,
	#[serde(default)]
	pub info_url: Option<Arc<str>>,
	#[serde(default)]
	pub aliases: Vec<Arc<str>>,
	#[serde(default="zero")]
	pub weight: i32,
	pub servers: Vec<SocketAddr>,
	pub protocol: DnsProtocol,
	pub tls_dns_name: Option<Arc<str>>,
	#[serde(skip_serializing)] //Don't leak our bind address to the outside
	pub bind_address: Option<SocketAddr>,
	#[serde(default="default_true", alias="trust_nx_responses")]
	pub trust_negative_responses: bool,
}

fn zero() -> i32 {
	return 0;
}

fn default_true() -> bool {
	return true;
}

impl Default for DnsConfig {
	fn default() -> Self {
		DnsConfig {
			allow_forward_lookup: true,
			allow_reverse_lookup: false,
			hidden_suffixes: Vec::new(),
			resolver: Default::default(),

			enable_system_resolver: true,
			system_resolver_name: "System".into(),
			system_resolver_weight: 1000,
			system_resolver_id: "system".into(),
		}
	}
}

impl From<DnsProtocol> for Protocol {
	fn from(value: DnsProtocol) -> Self {
		match value {
			DnsProtocol::Udp   => Protocol::Udp,
			DnsProtocol::Tcp   => Protocol::Tcp,
			DnsProtocol::Tls   => Protocol::Tls,
			DnsProtocol::Https => Protocol::Https,
			DnsProtocol::Quic  => Protocol::Quic,
		}
	}
}

impl DnsResolverConfig {
	pub fn to_hickory_resolver_config(
		&self
	) -> HickoryResolverConfig {
		let mut resolver = HickoryResolverConfig::new();
		for server in &self.servers {
			resolver.add_name_server(NameServerConfig{
				socket_addr: *server,
				protocol: self.protocol.clone().into(),
				tls_dns_name: self.tls_dns_name.clone().map(|s| s.to_string()),
				trust_negative_responses: self.trust_negative_responses,
				tls_config: None,
				bind_addr: self.bind_address,
			});
		}
		// Not configuring domain search here because searching
		// on the resolver level is a bad idea unless we are
		// taling about the system resolver which we
		// can't tell what to do (which is good!)
		return resolver;
	}
}

