# echoip-slatecave

This is a service inspired by ifconfig.co, but built from scratch with a more useful feature set. Currently live on [echoip.slatecave.net](https://echoip.slatecave.net).

It is Licensed under the AGPL-v3 license.

## Maintainence Mode

This project is in maintanance mode.

This means the following will happen:
* Dependency updates
* Bugfixes
* Small quality of life improvements

But no active feature development by Slatian.

## Building

Simply run `cargo build` after cloning. The binary should be called `target/debug/echoip-slatecave`.

To make a release build (the one you want to have on your server) run `cargo build --release`, the binary will end up in `target/release/echoip-slatecave`.

## Usage and configuration

Without any configuration `echoip-slatecave` will be readchable on http://localhost:3000 and use the `templates` folder relative to the pwd for its templates.

*Before running it on your server read the [security considerations](#security-considerations) first!*

### Commandline arguments

On the commandline `echoip-slatecave` accepts options for configuring the address to listen on and where to search for files overriding what is specified in the configuration file.

Use `echoip-slatecave --help` to get an up to date list.

### Configuration file

This section is about the general configration file you specify with `--config`.

You can find an example configuration file with sane defaults for a public deployment behind a proxy in the [echoip_config.toml](echoip_config.toml) file.

A less sane, but better for testing version can be found in [echoip_test.toml](echoip_test.toml).

### Templates

Templates in the templates folder exist for every rich page that `echoip-slatecave` supports.

There is a configuration file for templates which by default is the `extra.toml` file in the template directory. Its content is exposed to the templates in the `extra` struct.

The default templates should make use of everything exposed to the templating part, the `data.result` or `data` object is usually what you get when you ask for the json version.

In addition to that the following fields are accessible from inside the template:

`view`
: The views name (the basename of the template file, i.e. `404` or `ip`)

`format`
: The format name (`html`, `text`, `json`)

`mimetype`
: The resulting mimetype (i.e. `text/plain; charset=utf-8`)

`http_status`
: The numeric HTTP Status-Code at the time of rendering the template.

`language`
: The language requested by the browser.

`dns_resolvers`
: A list of [Selectable](src/settings.rs) structs representing the available DNS-Resolvers.

`dns_resolver_id`
: The id of the currently selected DNS-Resolver

The templates are covered by the AGPL as well, please share them with your users if you modified them.

### Geolocation databases

For geolocation to work you need a MaxMind format database, for full functionality you need the GeoLite2-ASN and GeoLite2-City databses. Unfortunately you have to sign up with [MaxMind](https://maxmind.com) to obtain them. Once you have a license key there is a helper script in [contrib/maxmind-download.sh](contrib/maxmind-download.sh) that helps you with keeping the databse updated.

As an alternative to MaxMind there is also [DB-IP who offer their free databases without a login](https://db-ip.com/db/lite.php). You want the City and ASN databases in mmdb format.

**Don't forget to set the atttribution in the template configuration appropriately.**

See the file `templates/extra.toml`.

Since v1.0 echoip-slatecave reloads the databses when it rececieves a `USR1` signal.

## Security Considerations

### Information disclosure
`echoip-slatecave` allows anyone with access to its web interface to place queries that result in multiple DNS lookups to your system configured DNS Sever. This can leak internal name mappings, the configuration file gives you some tools to limit what is exposed.

Most noably you can disable reverse dns lookups, hide domains with given suffixes and disable reverse lookup for ip-ranges that are classified as private or local.

### https and reverse proxy

`echoip-slatecave` only exposes an unencrypted http interface to keep the service itself simple.
For a public service you should use a reverse proxy like Caddy, apache2 or nginx and configure the `ip_header` option, see the echoip_config.toml file. Usually the preconfigured `RightmostXForwardedFor` is the correct one, but please doublecheck it matches your servers configuration, it should fail by simply not working, but no guarantees given.

Consider hiding the values of the following in your server logs for increased privacy:
* The `query` URL query paramter
* All paths subpath to `/ip/` and `/dig/`

### Denail of Service

`echoip-slatecave` has some simle ratelimiting built in (see the `[ratelimit]` section in the configuration file) this should help you with too frequest automated requests causung high load.
The default configuration is pretty liberal so that the average human probably won't notice the rate limit, but a misbehavingig bot will be limited to one request every 3 seconds after 15 requests.

## TODO

* [ ] Add a way to configure just the dns server addresses and derive the port from the protocol.
* [ ] Add an about page for the system resolver
* [ ] Expose DNS responses from the additional on the web interface

## License

Copyright (c) 2023 Slatian

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

This applies to all files (except for the configuration files, that would be nonsense) in this repository unless marked otherwise.


