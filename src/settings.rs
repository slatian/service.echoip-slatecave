use serde::{Deserialize,Serialize};

use lib_humus::HtmlTextJsonFormat;
use lib_humus::HumusQuerySettings;

use std::sync::Arc;

/* Response format */

pub type ResponseFormat = HtmlTextJsonFormat;

/* Query and Template Settings */

#[derive(Deserialize, Serialize, Clone)]
pub struct QuerySettings {
	pub format: ResponseFormat,
	pub lang: String,
	pub available_dns_resolvers: Vec<Selectable>,
	pub dns_resolver_id: Arc<str>,
	pub dns_disable_self_lookup: bool,
}

#[derive(Deserialize, Serialize, Clone)]
pub struct Selectable {
	pub id: Arc<str>,
	pub name: Arc<str>,
	pub weight: i32,
}

impl HumusQuerySettings<ResponseFormat> for QuerySettings {
	
	fn initalize_template_context(&self, context: &mut tera::Context) {
		context.insert("language", &self.lang);
		context.insert("dns_resolvers", &self.available_dns_resolvers);
		context.insert("dns_resolver_id", &self.dns_resolver_id);
	}

	fn get_format(&self) -> ResponseFormat {
		self.format.clone()
	}

}
